package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Author;

@Service
public class AuthorService {
    
    public static ArrayList<Author> getAuthorVN(){

        ArrayList<Author> authors = new ArrayList<Author>();

        Author authorsVN1 = new Author("authorsVN 1", "authorsVN@yahoo1.com","male 1");
        Author authorsVN2 = new Author("authorsVN 2", "authorsVN@yahoo2.com","male 2");
        Author authorsVN3 = new Author("authorsVN 3", "authorsVN@yahoo3.com","male 3");

        authors.add(authorsVN1);
        authors.add(authorsVN2);
        authors.add(authorsVN3);

        return authors;
    }
    public static ArrayList<Author> getAuthorJP(){
        ArrayList<Author>authors = new ArrayList<Author>();

        Author authorsJP1 = new Author("authorsKR 1", "authorsVN@gmail1.com","LGBT 1");
        Author authorsJP2 = new Author("authorsKR 2", "authorsVN@gmail2.com","LGBT 2");
        Author authorsJP3 = new Author("authorsKR 3", "authorsVN@gmail3.com","LGBT 3");

        authors.add(authorsJP1);
        authors.add(authorsJP2);
        authors.add(authorsJP3);

        return authors;
    }
    public static ArrayList<Author> getAuthorUS(){
        ArrayList<Author> authors = new ArrayList<Author>();

        Author authorsUS1 = new Author("authorsUS 1", "authorsVN@twitter1.com","female 1");
        Author authorsUS2 = new Author("authorsUS 2", "authorsVN@twitter2.com","female 2");
        Author authorsUS3 = new Author("authorsUS 3", "authorsVN@twitter3.com","female 3");

        authors.add(authorsUS1);
        authors.add(authorsUS2);
        authors.add(authorsUS3);

        return authors;
    }

    public static ArrayList<Author> getAuthorsAll() {
        ArrayList<Author> AuthorsAll = new ArrayList<Author>();

       AuthorsAll.addAll(getAuthorVN());
       AuthorsAll.addAll(getAuthorJP());
       AuthorsAll.addAll(getAuthorUS());
    
        return AuthorsAll;
        
    }
}

package com.example.demo.Controller;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Author;
import com.example.demo.Service.AuthorService;

@RestController
public class AuthorController {
    // đầu ra là 1 Class 
    @GetMapping("/author-info")
    public Author getEmailAuthor(@RequestParam("email") String email){
        ArrayList<Author> authors = AuthorService.getAuthorsAll();

        Author findAuthor = new Author();
        for(Author author : authors){
            if(author.getEmail().equals(email)){
                findAuthor = author ;
            }
        }
        return findAuthor;
    }

    // đầu ra là arraylist 
    @GetMapping("/author-gender")
    public ArrayList<Author> getGenderAuthor(@RequestParam("gender") String gender){
        ArrayList<Author> authors = AuthorService.getAuthorsAll();

        ArrayList<Author> findAuthor = new ArrayList<Author>();
        
        for(Author author : authors){
            if(author.getGender().equals(gender)){
                findAuthor.add(author);
            }
        }
        return findAuthor;
    }
}

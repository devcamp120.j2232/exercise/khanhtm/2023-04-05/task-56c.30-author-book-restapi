package com.example.demo.Service;

import java.util.ArrayList;


import org.springframework.stereotype.Service;


import com.example.demo.Model.Book;

@Service
public class BookService {
    
    public static ArrayList<Book> getAllBookList() {
        ArrayList<Book> bookList = new ArrayList<Book>();
        
        Book book1 = new Book("Nobita" , 5000.00 , 1000 );
        Book book2 = new Book("Pokemon",7000.000,2000 );
        Book book3 = new Book("Conan",400000.0000 , 50000 );

        book1.setAuthors(AuthorService.getAuthorVN());
        book2.setAuthors(AuthorService.getAuthorJP());
        book3.setAuthors(AuthorService.getAuthorUS());

        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);


        return bookList;

        }
    }
          
